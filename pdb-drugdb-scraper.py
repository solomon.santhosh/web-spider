import requests
from bs4 import BeautifulSoup as bs

def get_data(page_link):
    response = requests.get(page_link, headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0'})
    if response.status_code == 200:
        page_source = bs(response.content, 'html5lib')
        data_table = page_source.find_all('dl')
        if data_table:
            drug_data = []
            for data in data_table:
                headers = data.find_all('dt')
                for header in headers:
                    value = header.find_next('dl')
                    if value:
                        data_dict = dict()
                        data_dict[header.text] = value.text
                        drug_data.append(data_dict)

if __name__ == "__main__":
    url = 'https://www.drugbank.ca/drugs/DB01454'
    get_data(url)